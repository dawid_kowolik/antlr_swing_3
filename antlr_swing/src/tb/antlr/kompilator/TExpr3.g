tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer licznik_if = 0;
}
prog    : (e+=expr | d+=decl)* -> program(name={$e},deklaracje={$d})
    ;
decl :
        ^(VAR i1=ID) {globals.newSymbol($ID.text);} -> deklaracjaZmiennej(n={$ID.text})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr    : ^(PLUS  e1=expr e2=expr) -> dodaj(p1={$e1.st}, p2={$e2.st})
        | ^(MINUS e1=expr e2=expr) -> odejmij(p1={$e1.st}, p2 = {$e2.st})
        | ^(MUL   e1=expr e2=expr) -> mnoz(p1={$e1.st}, p2={$e2.st})
        | ^(DIV   e1=expr e2=expr) -> dziel(p1={$e1.st},p2={$e2.st})
        | ^(PODST i1=ID   e2=expr) {globals.hasSymbol($ID.text)}? -> zapisZmiennej( i={$i1.text}, wartosc={$e2.st})
        | ID         {globals.hasSymbol($ID.text)}? -> odczytZmiennej(i={$ID.text})
        | INT                       -> odczytLiczby(i={$INT.text})
        | ^(IF e1 = expr e2 = expr e3 =expr?) {licznik_if++;} -> if_else(warunek={$e1.st}, then_code ={$e2.st}, else_code ={$e3.st}, licznik ={licznik_if.toString()})
    ;
    